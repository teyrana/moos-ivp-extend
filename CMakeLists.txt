#=======================================================================
# FILE:  moos-ivp-extend/CMakeLists.txt
# DATE:  2012/07/24
# INFO:  Top-level CMakeLists.txt file for the moos-ivp-extend project
# NAME:  Maintained by Mike Benjamin - Original setup by Christian Convey
#        Chris Gagner, and tips borrowed from Dave Billin
#=======================================================================

cmake_minimum_required(VERSION 3.7)

PROJECT( MOOS_IVP_EXTEND )


# Set the output directories for the binary and library files
#=======================================================================
GET_FILENAME_COMPONENT(IVP_EXTEND_BIN_DIR "${CMAKE_SOURCE_DIR}/bin"  ABSOLUTE )
GET_FILENAME_COMPONENT(IVP_EXTEND_LIB_DIR "${CMAKE_SOURCE_DIR}/lib"  ABSOLUTE )
SET( LIBRARY_OUTPUT_PATH      "${IVP_EXTEND_LIB_DIR}" CACHE PATH "" )
SET( ARCHIVE_OUTPUT_DIRECTORY "${IVP_EXTEND_LIB_DIR}" CACHE PATH "" )
SET( LIBRARY_OUTPUT_DIRECTORY "${IVP_EXTEND_LIB_DIR}" CACHE PATH "" )
SET( EXECUTABLE_OUTPUT_PATH   "${IVP_EXTEND_BIN_DIR}" CACHE PATH "" )
SET( RUNTIME_OUTPUT_DIRECTORY "${IVP_EXTEND_BIN_DIR}" CACHE PATH "" )

SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake)


# Set System Specific Libraries
#=======================================================================
if(CMAKE_HOST_WIN32)
    message("--> Configuring For OS: Windows")
    list(APPEND SYSTEM_LIBS wsock32 )
elseif(CMAKE_HOST_UNIX)
    if(CMAKE_HOST_APPLE)
        message("-->Configuring For OS: UNIX/APPLE")
    elseif( CMAKE_SYSTEM_NAME STREQUAL "Linux")
        message("-->Configuring For OS: UNIX/LINUX")
    else()
        message("-->Configuring For OS: UNIX/Unknown")
    endif()

    list(APPEND SYSTEM_LIBS m pthread )
    find_package(Threads REQUIRED)
endif()

if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
    message("-->Configuring for arch: 'x86_64'")
    add_compile_options(-m64 -lm)
elseif(CMAKE_SYSTEM_PROCESSOR STREQUAL "arm")
    # NYI
    message(FATAL_ERROR "-->Configuring for arch: 'arm9'")
else()
    message(FATAL_ERROR "!! Unimplemented architecture?! ${CMAKE_SYSTEM_PROCESSOR}")
endif()


# ====== Find Boost library ======
#=======================================================================
if( ENABLE_BOOST )
    # Find necessary Boost components
    set(Boost_ADDITIONAL_VERSIONS "1.71" "1.71.0")
    set(Boost_USE_STATIC_LIBS ON)
    find_package(Boost COMPONENTS system thread regex filesystem REQUIRED)
    set( BOOST_LIBRARIES "${Boost_LIBRARIES}")   # alias to a consistent spelling.
    #message(STATUS "    >> Found Boost: ${Boost_VERSION}")
    #message(STATUS "    >> Include:     ${Boost_INCLUDE_DIRS}")
    #message(STATUS "    >> Link:        ${Boost_LIBRARIES}")
endif()


# Find MOOS
#=======================================================================
find_package(MOOS-IVP 10.0 REQUIRED)

INCLUDE_DIRECTORIES( ${MOOS_IVP_INCLUDE_PATHS} )
LINK_DIRECTORIES( ${MOOS_IVP_LINK_PATHS} )
# LINK_LIBRARIES( ${MOOS_IVP_LIBRARIES} )

## ====== ===== Show Found MOOS-IVP paths: ====== ======
message(STATUS "::STATUS::MOOS_FOUND:             ${MOOS_FOUND}")
message(STATUS "::STATUS::MOOS_IVP_FOUND:         ${MOOS_IVP_FOUND}")
message(STATUS "::STATUS::MOOS_IVP_BINARY_PATH:   ${MOOS_IVP_BINARY_PATHS}")
message(STATUS "::STATUS::MOOS_IVP_INCLUDE_PATHS: ${MOOS_IVP_INCLUDE_PATHS}")
message(STATUS "::STATUS::MOOS_IVP_LINK_PATHS:    ${MOOS_IVP_LINK_PATHS}")
message(STATUS "::STATUS::MOOS_IVP_LIBRARIES:     ${MOOS_IVP_LIBRARIES}")


# Start Configuring this code-base:
# =======================================================================

## ====== ===== Specify Compiler Flags ====== ======
set( CMAKE_CXX_FLAGS "-Wall")
set( CMAKE_CXX_FLAGS_DEBUG "-g -Og -DDEBUG")
set( CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG")


## ====== ===== Detect Supported Languages Versions ====== ======
include(CheckCXXCompilerFlag)
if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_COMPILER_IS_CLANG OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    check_cxx_compiler_flag(-std=c++11 SUPPORTS_CXX11)
    check_cxx_compiler_flag(-std=c++14 SUPPORTS_CXX14)
    check_cxx_compiler_flag(-std=c++17 SUPPORTS_CXX17)
    check_cxx_compiler_flag(-std=c++20 SUPPORTS_CXX20)
    check_cxx_compiler_flag(-std=c++23 SUPPORTS_CXX23)
 
    if(SUPPORTS_CXX23)
        set( CMAKE_CXX_STANDARD 23 )
    elseif(SUPPORTS_CXX20)
        set( CMAKE_CXX_STANDARD 20 )
    elseif(SUPPORTS_CXX17)
        set( CMAKE_CXX_STANDARD 17 )
    elseif(SUPPORTS_CXX14)
        set( CMAKE_CXX_STANDARD 14 )
    elseif(SUPPORTS_CXX11)
        set( CMAKE_CXX_STANDARD 11 )
    else()
        message(FATAL_ERROR "Compiler doesn't support C++11 -- This code uses C++11 features")
    endif()
    message("-->Configuring for C++${CMAKE_CXX_STANDARD}")
endif()

# Add Libraries and Applications
#=======================================================================
ADD_SUBDIRECTORY( src )
